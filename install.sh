#!/bin/sh
cd ~

#check if already exists
if [ -L .vimrc ]; then
        exit 1;
fi

#backup existing stuff
if [ -f .vimrc ]; then
        mv .vimrc .vimrc.bak
fi
if [ -d .vim ]; then
        mv .vim .vim.bak
fi
ln -s vim/.vimrc .vimrc
ln -s vim/.vim .vim

#fetch vundle
cd vim
git submodule init
git submodule update
