set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" " let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'StanAngeloff/php.vim'
Plugin 'scrooloose/nerdtree'

" " plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
" " plugin from http://vim-scripts.org/vim/scripts.html
" " Plugin 'L9'
" " Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" " git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" " The sparkup vim script is in a subdirectory of this repo called vim.
" " Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" " Install L9 and avoid a Naming conflict if you've already installed a
" " different version somewhere else.
" " Plugin 'ascenator/L9', {'name': 'newL9'}
"
call vundle#end()

filetype plugin indent on


" Do smart autoindenting
set smartindent
set autoindent
set nowrap

" Use UTF-8 encoding
set encoding=utf-8

" The ruler shows line, column numbers
" and relative position of the cursor
set ruler

" Show line numbers
"set number

" Show in what mode we are
set showmode
" Show command entered in bottom right
set showcmd
" Remember the last 100 commands on the command-line
set history=100
" Hide buffers instead of unloading them
" If we unload a buffer it means it's local history/undo stack
" will be gone if we switch buffers
set hidden

" Tab settings
" One tab is 4 space
"set tabstop=4
" Number of spaces for (auto)indent
"set shiftwidth=4
" Round indent to multiple of shiftwidth
"set shiftround
" Expand tabs to spaces
"set expandtab
"set softtabstop=4

" Syntax!
" Turn on syntax highlightning
syntax on
colorscheme default
set background=dark

" Turn on command-line completion
set wildmenu
" List all matches and complete till longest common string
set wildmode=full

" Always have a status line
set laststatus=2

" Hide mouse when typing
set mousehide

" When scrolling up/down the page, number of lines when scrolling starts
set scrolloff=8

" ==== End display settings ====

" Make backspace behave normally
set backspace=2

" Space, the Leader!
let mapleader="\<space>"

" ==== Search/yank settings ====
" Yank/delete copies to system clipboard
set clipboard=unnamed

" Search settings
" Ignore case when searching
set ignorecase
" Ignore ignorecase when search pattern contains uppercase chars
set smartcase
" Incremental search - start highlighting search results as the
" search string is typed
set incsearch
" Highlight search results
set hlsearch

" Normal, non-recursive map
" Clear the search buffer
nnoremap <leader>/ :noh<cr>

" Make n always search forward
" Make N always search backward
nnoremap <expr> n 'Nn'[v:searchforward]
nnoremap <expr> N 'nN'[v:searchforward]

" ==== End search/yank settings ====


"" ==== Plugins settings ====
"" ==== VIM PyQt5 Importer settings ====
"map <leader>pi :PyQt5ImportClass<cr>
"" ==== End VIM PyQt5 Importer settings ====
""
"" ==== VIM Argument Swapper settings ====
"map <leader>as :ArgumentSwapperSwap<cr>
"" ==== End VIM Argument Swapper settings ====
""
"" ==== NERDTree settings ====
"nnoremap <silent> <c-n> :NERDTreeToggle<CR>
"let NERDTreeMapHelp='<f1>'
"let NERDTreeIgnore = ['\.pyc$', '__init__.py', '__pycache__']
"" ==== End NERDtree settings ====
""
"" ==== CtrlP settings ====
"" Open CtrlP buffers
"map <tab> :CtrlPBuffer<cr>
"map <leader>tb :CtrlPBufTag<cr>
"map <leader>ta :CtrlPTag<cr>
"" Jump to definition
"map <silent> <leader>jd :CtrlPTag<cr><C-\>w
"" Things to ignore with CtrlP
"let g:ctrlp_user_command = 'ag %s -l --nocolor -g "" '
"" uses ~/.agignore
""let g:ctrlp_custom_ignore = 'vendor/\|tests\/log\|git\|env\|build/\|dist/\|__pycache__\|docs\/build/\|public_html\/api/\|public_html\/docs/\|*.pyc'
"let g:ctrlp_match_current_file = 1
"let g:ctrlp_lazy_update = 1
"let g:ctrlp_extensions = ['tag', 'buffertag']
"let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }
"let g:ctrlp_use_caching = 0
"" ==== End CtrlP settings ====
""
"" ==== Easymotion settings ====
"" Disable default easymotion mappings
"let g:EasyMotion_do_mapping = 0
"" Turn on case insensitive feature
"let g:EasyMotion_smartcase = 1
"nmap <leader>s <Plug>(easymotion-s)
"vmap <leader>s <Plug>(easymotion-s)
"" ==== End Easymotion settings ====
""
"" ==== hardtime settings ====
"" Turn on hardtime
"let g:hardtime_default_on = 0
"" Allow different movement key
"let g:hardtime_allow_different_key = 1
"" Max 3 of the same movement allowed at a time
"let g:hardtime_maxcount = 3
"" ==== End hardtime settings ====
""
"" ==== rooter settings ====
"let g:rooter_silent_chdir=1
"let g:rooter_patterns = ['composer.json', 'Vagrantfile', '.git/']
"" ==== End rooter settings ====
""
"" ==== PreserveNoEOL settings ====
"let g:PreserveNoEOL=0
"" ==== End Preserver NoEOL settings ====
""
"" ==== tagbar settings ====
"" Open tagbar with F8
"nnoremap <F8> :TagbarToggle<CR>
"" Location of phpctags bin
"let g:tagbar_phpctags_bin='~/.vim/phpctags'
"" ==== End tagbar settings ====
""
"" ==== gutentags settings ====
"" Exclude css, html, js files from generating tag files
"let g:gutentags_exclude = ['*.css', '*.html', '*.js', '*.json', '*.xml',
"                            \ '*.phar', '*.ini', '*.rst', '*.md',
"                            \ '*vendor/*/test*', '*vendor/*/Test*',
"                            \ '*vendor/*/fixture*', '*vendor/*/Fixture*',
"                            \ '*var/cache*', '*var/log*']
"" Where to store tag files
"let g:gutentags_cache_dir = '~/.vim/gutentags'
"" ==== End gutentags settings ====
""
"let g:ale_linters = {
"\   'php': ['php'],
"\}
"let g:ale_lint_on_save = 1
"let g:ale_lint_on_text_changed = 0
"" ==== lightline settings ====
"let g:lightline = {
"    \ 'active': {
"    \   'left': [['mode'], ['readonly', 'filename', 'modified'], ['tagbar', 'ale', 'gutentags']],
"    \   'right': [['lineinfo'], ['filetype']]
"    \ },
"    \ 'inactive': {
"    \   'left': [['absolutepath']],
"    \   'right': [['lineinfo'], ['filetype']]
"    \ },
"    \ 'component': {
"    \   'lineinfo': '%l\%L [%p%%], %c, %n',
"    \   'tagbar': '%{tagbar#currenttag("[%s]", "", "f")}',
"    \   'ale': '%{ale#statusline#Status()}',
"    \   'gutentags': '%{gutentags#statusline("[Generating...]")}'
"    \ },
"    \ }
"" ==== End lightline settings ====
""
"" ==== UltiSnips settings ====
"let g:UltiSnipsExpandTrigger="<tab>"
"let g:UltiSnipsSnippetDirectories=[$HOME.'/.vim/UltiSnips', $HOME.'/.vim/bundle/sniphpets-common/UltiSnips', $HOME.'/.vim/bundle/sniphpets']
"" ==== End UltiSnips settings ====
""
"" ==== vim side search settings ====
"let g:side_search_prg = 'ag --word-regexp --heading --stats -B 1 -A 4 --ignore="*.js"'
"let g:side_search_splitter = 'vnew'
"let g:side_search_split_pct = 0.5
"nnoremap <leader>gw :SideSearch <C-r><C-w><CR> | wincmd p
"cabbrev SS SideSearch
"" ==== End vim side search settings ====
""
"" ==== vim-php-namespace settings ====
"function! IPhpInsertUse()
"    call PhpInsertUse()
"    call feedkeys('a',  'n')
"endfunction
"autocmd FileType php inoremap <Leader>pnu <Esc>:call IPhpInsertUse()<CR>
"autocmd FileType php noremap <Leader>pnu :call PhpInsertUse()<CR>
"function! IPhpExpandClass()
"    call PhpExpandClass()
"    call feedkeys('a', 'n')
"endfunction
"autocmd FileType php inoremap <Leader>pne <Esc>:call IPhpExpandClass()<CR>
"autocmd FileType php noremap <Leader>pne :call PhpExpandClass()<CR>
"autocmd FileType php inoremap <Leader>pns <Esc>:call PhpSortUse()<CR>
"autocmd FileType php noremap <Leader>pns :call PhpSortUse()<CR>
"let g:php_namespace_sort_after_insert=1
"
"" ==== End plugin settings ====
"
"" ==== Automatic ====
"" Automatically change cwd to the directory of the file in the current buffer
"autocmd BufEnter * silent! lcd %:p:h
"
"" Set file types for some of the PHP related extensions
"autocmd BufNewFile,BufRead *.inc set ft=php
"autocmd BufNewFile,BufRead *.phtml set ft=phtml
"autocmd BufNewFile,BufRead *.tpl set ft=phtml
"
"" since installing vim-indent-object
"" PHP block comments got broken
"" this whatever is fixes it
"" looks like this is not needed anymore
"" granted, not using vim-indent-object
"" au FileType php setlocal comments=s1:/*,mb:*,ex:*/,://,:#
"" au FileType php setlocal formatoptions+=cro
"
"autocmd BufNewFile,BufRead *.xsd set ft=xml
"let g:xml_syntax_folding=1
"au FileType xml setlocal foldmethod=syntax
"
"au BufRead,BufNewFile *.twig set ft=html
"" ==== End automatic ====

" ==== Remappings ====

" auto source .vimrc
autocmd! bufwritepost .vimrc source %

" check with ctrl+l the current file for php syntax errors
autocmd FileType php noremap <C-l> :!/usr/bin/php -l %<cr>

" Highlight word under cursor
nnoremap <silent> <Leader>* :let @/='\<<C-R>=expand("<cword>")<CR>\>'<CR>:set hls<CR>

" ==== End remappings ====
